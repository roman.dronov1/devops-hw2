FROM python:latest
WORKDIR /godot
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true
RUN apt-get update
RUN apt-get install clang -y
RUN apt-get install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev \
    libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm -y
RUN sed -i "1c\#! /usr/bin/python3" /usr/bin/scons
EXPOSE 5000
COPY . .
#RUN scons platform=x11 tools=no target=release_debug bits=64 use_llvm=yes
#RUN scons -j8 platform=x11
CMD tail -f /dev/null