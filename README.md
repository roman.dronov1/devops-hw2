# DevOps Task 2 Gitlab+docker+godot
<p align="center">
  <a href="https://godotengine.org">
    <img src="logo.svg" width="400" alt="Godot Engine logo">
  </a>
</p>

## ENVIROMENT
* Debian-buster
* Docker
* Docker-compose v3
* Gitlab image

## GITLAB BUILD SETUP
### Enviroment
* Phyton image
* Stages
  * build
  * test
  * package
### Description
#### Why phyton image?
Gitlab uses docker to test commits. Phyton image comes installed on debian buster OS, which make setup easier. The only minus - scons adapted to phyton 2 verison. Which was resolved using configuration update and manual file change for scons.
### Build stage
Build stage uses Clang to build Godot faster
### Test stage
Test stage configured to use artifacts from the build stage to make test build efficient.
### Package stage
Packing build artifacts

## Docker-compose for local Gitlab deployment
Using two services, both formed from images.
### Web service (gitlab)
* image: gitlab/gitlab-ee:latest

To make runner setup easier used **[Docker network aliases](https://docs.docker.com/compose/compose-file/#aliases) , what helps to udentify docker containers by the custom name, because IP usualy changes inside docker network a lot.
### Runner (gitlab-runner)
* image: gitlab/gitlab-runner:latest

Runner should be registred. There is two possible ways. 
However both require to push project at first to find out token for runner registration.
#### Gitlab runner file configuration changing
File config.toml in **runner-settings** is linked to runner container.


```yaml
volumes:
      - './runner-settings:/etc/gitlab-runner'
```


Which alows to make all changes on scratch.
To change token use line

```toml
 token = [TOKEN]
```

URL presented as http://gitlab which has been decribed.
#### Manual registration using docker exec
Another option is to register using docker exec command manually.

```bash
docker exec -it [CONTAINER_NAME] gitlab-runner register \
--non-interactive \
--url http://gitlab \
--registration-token [TOKEN] \
--executor docker \
--docker-image alpine:latest \
--docker-network-mode gitlab-net
```

The only extra thing than need to be done is discovering of container name in docker network.
#### Network problem solution
The problem has been affected because docker and gitlab runner had different networks. The solution is to add extra line which defines network.
* For config.toml file.  In **[runners.docker]** section.

```toml
network_mode = "gitlab-net"
```

* For manual change to add to console command.

```bash
--docker-network-mode gitlab-net
```

Network names should correspond to docker-compose network name.

### What is Dockerfile for
Dockerfile has been used to test commands and build configuration for gitlab setup as the fastest way with possibility to get inside container and control everything.